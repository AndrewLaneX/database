use crate::{
    glossary::*,
    parse_error::{Kind, ParseError},
};
use comrak::{
    arena_tree::Node,
    nodes::{Ast, ListType, NodeHtmlBlock, NodeValue},
    parse_document, Arena, ComrakOptions,
};
use serde::Deserialize;
use std::{cell::RefCell, convert::AsRef, fs, iter::once, path::Path};

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct Info {
    language_name: String,
    title: String,
    search: String,
    about: String,
}

pub fn parse<P: AsRef<Path>>(path: P) -> Result<Glossary, Vec<ParseError>> {
    let mut errors = Vec::new();
    let info_path = path.as_ref().join("info.toml");
    let info: Option<Info> = if let Ok(info) = fs::read_to_string(&info_path) {
        match toml::from_str(&info) {
            Ok(info) => Some(info),
            Err(inner) => {
                errors.push(ParseError {
                    path: info_path.to_owned(),
                    line: 0,
                    kind: Kind::InfoTomlParseError { inner },
                });
                None
            }
        }
    } else {
        errors.push(ParseError {
            path: info_path.to_owned(),
            line: 0,
            kind: Kind::NoInfoToml,
        });
        None
    };

    let dirs = if let Ok(dirs) = fs::read_dir(path) {
        dirs
    } else {
        return Err(errors);
    };
    let mut sections: Vec<_> = dirs
        .filter_map(|entry| entry.ok())
        .filter(|entry| {
            entry
                .path()
                .extension()
                .map_or(false, |extension| extension == "md")
        })
        .collect();

    sections.sort_unstable_by(|a, b| {
        let a_index = parse_section_index(&a.path());
        let b_index = parse_section_index(&b.path());

        a_index.partial_cmp(&b_index).unwrap()
    });

    let sections = sections
        .into_iter()
        .map(|entry| parse_section(&entry.path()))
        .filter_map(|result| match result {
            Ok(parsed) => Some(parsed),
            Err(error) => {
                errors.extend(error);
                None
            }
        })
        .collect::<Vec<_>>();

    let sections = if errors.is_empty() {
        Some(sections)
    } else {
        None
    };

    if let (Some(info), Some(sections)) = (info, sections) {
        Ok(Glossary {
            language_name: info.language_name,
            title: info.title,
            search: info.search,
            about: info.about,
            sections,
        })
    } else {
        Err(errors)
    }
}

fn parse_section_index(path: &Path) -> u8 {
    path.file_name()
        .unwrap()
        .to_string_lossy()
        .split('.')
        .next()
        .unwrap()
        .parse::<u8>()
        .unwrap()
}

fn parse_section(path: &Path) -> Result<Section, Vec<ParseError>> {
    let file = fs::read_to_string(path).unwrap();
    let arena = Arena::new();
    let root = parse_document(
        &arena,
        &file,
        &ComrakOptions {
            ext_strikethrough: true,
            ext_autolink: true,
            ext_superscript: true,
            ..Default::default()
        },
    );

    parse_subsection(path, root.children())
}

fn parse_subsection<'a, I>(
    path: &Path,
    mut children: I,
) -> Result<Section, Vec<ParseError>>
where
    I: Iterator<Item = &'a Node<'a, RefCell<Ast>>>,
{
    let heading = children.next().ok_or_else(|| {
        vec![ParseError {
            path: path.to_owned(),
            line: 0,
            kind: Kind::EmptyFile,
        }]
    })?;
    let data = heading.data.borrow();
    let mut errors = Vec::new();
    let current_level = if let NodeValue::Heading(heading) = data.value {
        Some(heading.level as usize)
    } else {
        errors.push(ParseError {
            path: path.to_owned(),
            line: 0,
            kind: Kind::FileStartsWithNotHeading,
        });
        None
    };
    let title = String::from_utf8(data.content.clone()).unwrap();

    let contents = if let Some(current_level) = current_level {
        parse_contents(path, children, current_level)
    } else {
        parse_contents(path, once(heading).chain(children), 1)
    };

    let contents = match contents {
        Ok(contents) => {
            if errors.is_empty() {
                Some(contents)
            } else {
                None
            }
        }
        Err(error) => {
            errors.extend(error);
            None
        }
    };

    contents
        .map(|contents| Section { title, contents })
        .ok_or(errors)
}

fn parse_contents<'a, I>(
    path: &Path,
    children: I,
    current_level: usize,
) -> Result<Vec<Contents>, Vec<ParseError>>
where
    I: Iterator<Item = &'a Node<'a, RefCell<Ast>>>,
{
    let mut contents = Vec::new();
    let mut children = children.peekable();
    let mut errors = Vec::new();

    while let Some(child) = children.peek() {
        let data = child.data.borrow();
        match &data.value {
            NodeValue::Paragraph => {
                let paragraph = parse_paragraph(child.children());
                contents.push(Contents::Paragraph(paragraph));
            }
            NodeValue::List(list) => {
                let items = child
                    .children()
                    .map(|item| {
                        parse_contents(path, item.children(), current_level)
                    })
                    .filter_map(|result| match result {
                        Ok(result) => Some(result),
                        Err(error) => {
                            errors.extend(error);
                            None
                        }
                    })
                    .collect();

                let start = match list.list_type {
                    ListType::Bullet => None,
                    ListType::Ordered => Some(list.start),
                };

                contents.push(Contents::List { start, items });
            }
            NodeValue::Heading(heading) => {
                let start_level = heading.level;

                let mut subsection_children = Vec::new();
                let mut is_first_heading = true;

                while let Some(&child) = children.peek() {
                    let value = &child.data.borrow().value;
                    if let NodeValue::Heading(heading) = value {
                        if is_first_heading || heading.level > start_level {
                            is_first_heading = false;
                            subsection_children.push(child);
                        } else {
                            break;
                        }
                    } else {
                        subsection_children.push(child);
                    }

                    children.next();
                }

                match parse_subsection(path, subsection_children.into_iter()) {
                    Ok(subsection) => {
                        contents.push(Contents::Subsection(subsection))
                    }
                    Err(error) => errors.extend(error),
                }
                continue;
            }
            NodeValue::HtmlBlock(html) => {
                let result = parse_html(
                    path,
                    html,
                    data.start_line as usize - 1,
                    current_level as usize + 1,
                    &mut children,
                );

                match result {
                    Ok(content) => contents.push(content),
                    Err(error) => errors.extend(error),
                }
                continue;
            }
            _ => unreachable!(),
        }

        children.next();
    }

    if errors.is_empty() {
        Ok(contents)
    } else {
        Err(errors)
    }
}

fn parse_paragraph<'a, I>(children: I) -> Vec<ParagraphItem>
where
    I: Iterator<Item = &'a Node<'a, RefCell<Ast>>>,
{
    let mut paragraph = Vec::new();

    for child in children {
        match &child.data.borrow().value {
            NodeValue::Text(text) => {
                let text = String::from_utf8(text.clone()).unwrap();
                if let Some(ParagraphItem::Text(previous)) =
                    paragraph.last_mut()
                {
                    previous.push_str(&text);
                } else {
                    paragraph.push(ParagraphItem::Text(text));
                }
            }
            NodeValue::SoftBreak => {
                if let Some(ParagraphItem::Text(text)) = paragraph.last_mut() {
                    text.push(' ');
                }
            }
            NodeValue::LineBreak => {
                paragraph.push(ParagraphItem::LineBreak);
            }
            NodeValue::Link(link) => {
                let text = parse_paragraph(child.children());
                let url = String::from_utf8(link.url.clone()).unwrap();
                paragraph.push(ParagraphItem::Link { text, url });
            }
            NodeValue::Emph => {
                let text = parse_paragraph(child.children());
                paragraph.push(ParagraphItem::Italic(text));
            }
            NodeValue::Strong => {
                let text = parse_paragraph(child.children());
                paragraph.push(ParagraphItem::Bold(text));
            }
            NodeValue::Strikethrough => {
                let text = parse_paragraph(child.children());
                paragraph.push(ParagraphItem::Strikethrough(text));
            }
            NodeValue::Superscript => {
                let text = parse_paragraph(child.children());
                paragraph.push(ParagraphItem::Superscript(text));
            }
            NodeValue::Code(code) => {
                let text = String::from_utf8(code.clone()).unwrap();
                paragraph.push(ParagraphItem::Monospace(text));
            }
            NodeValue::Image(image) => {
                let url = String::from_utf8(image.url.clone()).unwrap();
                let alt = String::from_utf8(image.title.clone()).unwrap();
                paragraph.push(ParagraphItem::Image { url, alt })
            }
            _ => unreachable!(),
        }
    }

    paragraph
}

fn parse_html<'a, I>(
    path: &Path,
    html: &NodeHtmlBlock,
    start_line: usize,
    expected_level: usize,
    mut children: I,
) -> Result<Contents, Vec<ParseError>>
where
    I: Iterator<Item = &'a Node<'a, RefCell<Ast>>>,
{
    let mut errors = Vec::new();
    if html.literal.starts_with(b"<glossary-variable") {
        let string = String::from_utf8(html.literal.clone()).unwrap();
        let mut parts = string.split_whitespace().skip(1);

        let color = match parts.next() {
            Some("color=\"red\">") => Some(Color::Red),
            Some("color=\"pink\">") => Some(Color::Pink),
            Some("color=\"purple\">") => Some(Color::Purple),
            Some("color=\"deepPurple\">") => Some(Color::DeepPurple),
            Some("color=\"indigo\">") => Some(Color::Indigo),
            Some("color=\"blue\">") => Some(Color::Blue),
            Some("color=\"lightBlue\">") => Some(Color::LightBlue),
            Some("color=\"cyan\">") => Some(Color::Cyan),
            Some("color=\"teal\">") => Some(Color::Teal),
            Some("color=\"green\">") => Some(Color::Green),
            Some("color=\"lightGreen\">") => Some(Color::LightGreen),
            Some("color=\"lime\">") => Some(Color::Lime),
            Some("color=\"yellow\">") => Some(Color::Yellow),
            Some("color=\"amber\">") => Some(Color::Amber),
            Some("color=\"orange\">") => Some(Color::Orange),
            Some("color=\"deepOrange\">") => Some(Color::DeepOrange),
            Some("color=\"brown\">") => Some(Color::Brown),
            Some("color=\"gray\">") => Some(Color::Gray),
            Some("color=\"blueGray\">") => Some(Color::BlueGray),
            Some(..) => {
                errors.push(ParseError {
                    path: path.to_owned(),
                    line: start_line,
                    kind: Kind::InvalidVariableColor,
                });
                None
            }
            None => {
                errors.push(ParseError {
                    path: path.to_owned(),
                    line: start_line,
                    kind: Kind::InvalidVariableOpeningTag,
                });
                return Err(errors);
            }
        };

        let _ = children.next();

        let heading = if let Some(heading) = children.next() {
            heading
        } else {
            errors.push(ParseError {
                path: path.to_owned(),
                line: start_line,
                kind: Kind::NoVariableName { expected_level },
            });
            return Err(errors);
        };

        let data = heading.data.borrow();
        let name = if let NodeValue::Heading(heading_data) = &data.value {
            let current_level = heading_data.level as usize;
            if current_level != expected_level {
                errors.push(ParseError {
                    path: path.to_owned(),
                    line: data.start_line as usize - 1,
                    kind: Kind::WrongHeadingLevel {
                        current_level,
                        expected_level,
                    },
                });
            }

            String::from_utf8(data.content.clone()).ok()
        } else {
            errors.push(ParseError {
                path: path.to_owned(),
                line: start_line,
                kind: Kind::NoVariableName { expected_level },
            });
            None
        };

        let prefix = if name.is_none() { Some(heading) } else { None };

        let variable_children: Vec<_> = prefix
            .into_iter()
            .chain(&mut children)
            .take_while(|node| match &node.data.borrow().value {
                NodeValue::HtmlBlock(html) => {
                    html.literal != b"</glossary-variable>\n"
                }
                _ => true,
            })
            .collect();

        let description = parse_contents(
            path,
            variable_children.into_iter(),
            expected_level + 1,
        );
        let description = match description {
            Ok(description) => Some(description),
            Err(error) => {
                errors.extend(error);
                None
            }
        };

        if let (Some(name), Some(color), Some(description)) =
            (name, color, description)
        {
            Ok(Contents::Variable {
                name,
                color,
                description,
            })
        } else {
            Err(errors)
        }
    } else if html.literal == b"<figure>\n" {
        let _ = children.next();
        let paragraph = if let Some(paragraph) = children.next() {
            paragraph
        } else {
            errors.push(ParseError {
                path: path.to_owned(),
                line: start_line,
                kind: Kind::NoFigureImage,
            });
            return Err(errors);
        };
        // If there is no first child, there would be no paragraph either
        let image = paragraph.children().nth(0).unwrap();
        let image_data = image.data.borrow();
        let image_data = if let NodeValue::Image(image) = &image_data.value {
            let url = String::from_utf8(image.url.clone()).unwrap();
            let title = String::from_utf8(image.title.clone()).unwrap();

            Some((url, title))
        } else {
            errors.push(ParseError {
                path: path.to_owned(),
                line: start_line,
                kind: Kind::NoFigureImage,
            });
            None
        };

        let figcaption_start = if let Some(child) = children.next() {
            child
        } else {
            errors.push(ParseError {
                path: path.to_owned(),
                line: paragraph.data.borrow().start_line as usize - 1,
                kind: Kind::NoFigcaption,
            });
            return Err(errors);
        };

        let figcaption_start_data = figcaption_start.data.borrow();
        match &figcaption_start_data.value {
            NodeValue::HtmlBlock(x) if x.literal == b"<figcaption>\n" => {}
            _ => {
                errors.push(ParseError {
                    path: path.to_owned(),
                    line: figcaption_start_data.start_line as usize - 1,
                    kind: Kind::NoFigcaption,
                });
            }
        };

        let paragraph = if let Some(paragraph) = children.next() {
            paragraph
        } else {
            errors.push(ParseError {
                path: path.to_owned(),
                line: figcaption_start_data.start_line as usize - 1,
                kind: Kind::NoCaption,
            });
            return Err(errors);
        };
        let paragraph_data = paragraph.data.borrow();
        let description = match paragraph_data.value {
            NodeValue::Paragraph => Some(parse_paragraph(paragraph.children())),
            _ => {
                errors.push(ParseError {
                    path: path.to_owned(),
                    line: paragraph_data.start_line as usize - 1,
                    kind: Kind::InvalidCaption,
                });
                None
            }
        };

        let figcaption_end = if let Some(end) = children.next() {
            end
        } else {
            errors.push(ParseError {
                path: path.to_owned(),
                line: paragraph_data.start_line as usize - 1,
                kind: Kind::InvalidFigureEnd {},
            });
            return Err(errors);
        };
        let figcaption_end_data = figcaption_end.data.borrow();
        match &figcaption_end_data.value {
            NodeValue::HtmlBlock(html)
                if html.literal == b"</figcaption>\n</figure>\n" => {}
            _ => {
                errors.push(ParseError {
                    path: path.to_owned(),
                    line: figcaption_end_data.start_line as usize - 1,
                    kind: Kind::InvalidFigureEnd,
                });
                return Err(errors);
            }
        }

        if let (Some((url, alt)), Some(description)) = (image_data, description)
        {
            Ok(Contents::Image {
                url,
                alt,
                description,
            })
        } else {
            Err(errors)
        }
    } else {
        Err(vec![ParseError {
            path: path.to_owned(),
            line: start_line,
            kind: Kind::UnknownHtml,
        }])
    }
}
