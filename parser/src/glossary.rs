#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Glossary {
    pub language_name: String,
    pub title: String,
    pub search: String,
    pub about: String,
    pub sections: Vec<Section>,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Section {
    pub title: String,
    pub contents: Vec<Contents>,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum Contents {
    Subsection(Section),
    Paragraph(Vec<ParagraphItem>),
    Variable {
        name: String,
        color: Color,
        description: Vec<Contents>,
    },
    Image {
        url: String,
        alt: String,
        description: Vec<ParagraphItem>,
    },
    List {
        start: Option<usize>,
        items: Vec<Vec<Contents>>,
    },
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum ParagraphItem {
    LineBreak,
    Text(String),
    Bold(Vec<ParagraphItem>),
    Italic(Vec<ParagraphItem>),
    Monospace(String),
    Strikethrough(Vec<ParagraphItem>),
    Superscript(Vec<ParagraphItem>),
    Link {
        text: Vec<ParagraphItem>,
        url: String,
    },
    Image {
        alt: String,
        url: String,
    },
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum Color {
    Red,
    Pink,
    Purple,
    DeepPurple,
    Indigo,
    Blue,
    LightBlue,
    Cyan,
    Teal,
    Green,
    LightGreen,
    Lime,
    Yellow,
    Amber,
    Orange,
    DeepOrange,
    Brown,
    Gray,
    BlueGray,
}
