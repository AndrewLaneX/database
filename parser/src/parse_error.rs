use inflector::Inflector;
use std::{
    io::Write,
    iter::once,
    ops::{RangeBounds, RangeFull},
    path::PathBuf,
};
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

const INVALID_VARIABLE_TAG_HELP: &str =
    "The opening tag must look like `<glossary-variable color=\"{color}\">`, \
     where `{color}` is one of these: `red`, `pink`, `purple`, `deepPurple`, \
     `indigo`, `blue`, `lightBlue`, `cyan`, `teal`, `green`, `lightGreen`, \
     `lime`, `yellow`, `amber`, `orange`, `deepOrange`, `brown`, `gray`, \
     `blueGray`";

const ALLOWED_HTML_MARKUP: [&str; 2] = [
    "<glossary-variable color=\"{{color}}\">...</glossary-variable>",
    "<figure> ![]() <figcaption>...</figcaption></figure>",
];

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ParseError {
    pub path: PathBuf,
    // Must be 0-based
    pub line: usize,
    pub kind: Kind,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Kind {
    NoInfoToml,
    InfoTomlParseError {
        inner: toml::de::Error,
    },
    EmptyFile,
    FileStartsWithNotHeading,
    InvalidVariableOpeningTag,
    InvalidVariableColor,
    NoVariableName {
        expected_level: usize,
    },
    WrongHeadingLevel {
        current_level: usize,
        expected_level: usize,
    },
    NoFigureImage,
    NoFigcaption,
    NoCaption,
    InvalidCaption,
    InvalidFigureEnd,
    UnknownHtml,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct Highlight<R: RangeBounds<usize>> {
    range: R,
    color: Color,
}

impl ParseError {
    pub fn display(&self) {
        let stderr = &mut StandardStream::stderr(ColorChoice::Always);

        if self.kind == Kind::NoInfoToml {
            print_error(stderr, "Could not find `info.toml`");
            self.print_path(stderr);
            print_help(
                stderr,
                "Create a file `info.toml` with the following contents:",
            );
            print_file::<_, RangeFull>(
                stderr,
                "language_name = \"...\"\n\
                 title = \"...\"\n\
                 search = \"...\"
                 about = \"...\""
                    .lines(),
                0,
                None,
            );
            return;
        }

        if self.kind == Kind::EmptyFile {
            print_error(stderr, "Empty section file");
            self.print_path(stderr);
            print_help(
                stderr,
                "Start the section with a heading and contents:",
            );
            print_file::<_, RangeFull>(
                stderr,
                "# Glossary of Android Telegram Themes variables\n\n\

                While creating themes for Android Telegram..."
                    .lines(),
                0,
                None,
            );
            return;
        }

        let file = std::fs::read_to_string(&self.path).unwrap();
        let display_original = |stderr| {
            let start = self.line.saturating_sub(2);

            print_file(
                stderr,
                file.lines().skip(start),
                start,
                Some(Highlight {
                    range: self.line..=self.line,
                    color: Color::Red,
                }),
            );
        };

        match &self.kind {
            Kind::InfoTomlParseError { inner } => {
                let message = format!(
                    "Failed to parse `info.toml`: {}",
                    inner.to_string()
                );
                print_error(stderr, &message);
                self.print_path(stderr);

                let line_col = inner.line_col();
                let (lines, start, error) = if let Some((line, ..)) = line_col {
                    let start = line.saturating_sub(2);
                    let lines: Vec<_> =
                        file.lines().skip(start).take(6).collect();
                    let highlight = Highlight {
                        range: line..=line,
                        color: Color::Red,
                    };
                    (lines, start, Some(highlight))
                } else {
                    (file.lines().take(6).collect(), 0, None)
                };

                print_file(stderr, lines.into_iter(), start, error);
            }
            Kind::FileStartsWithNotHeading => {
                print_error(
                    stderr,
                    "Section file doesn't start with a heading",
                );
                self.print_path(stderr);
                display_original(stderr);
                print_help(stderr, "Add a first-level heading:");
                let stem = self.path.file_stem().unwrap().to_string_lossy();
                let name = stem.split('.').nth(1).unwrap();
                let heading = format!("# {}", name.to_sentence_case());

                print_file(
                    stderr,
                    [heading.as_str(), ""].iter().copied().chain(file.lines()),
                    0,
                    Some(Highlight {
                        range: 0..=0,
                        color: Color::Green,
                    }),
                )
            }
            Kind::InvalidVariableOpeningTag => {
                print_error(stderr, "Invalid <glossary-variable> opening tag");
                self.print_path(stderr);
                display_original(stderr);
                print_help(stderr, INVALID_VARIABLE_TAG_HELP);
            }
            Kind::InvalidVariableColor => {
                print_error(stderr, "Unknown variable color");
                self.print_path(stderr);
                display_original(stderr);
                print_help(stderr, INVALID_VARIABLE_TAG_HELP);
            }
            &Kind::NoVariableName { expected_level } => {
                print_error(
                    stderr,
                    "Expected a variable name in <glossary-variable>",
                );
                let start = self.line.saturating_sub(2);
                self.print_path(stderr);
                display_original(stderr);

                print_help(stderr, "Add a heading with the variable name:");
                let tag_line = file.lines().nth(self.line).unwrap();
                let heading =
                    format!("{} {{variableName}}", "#".repeat(expected_level));
                print_file(
                    stderr,
                    [tag_line, "", &heading]
                        .iter()
                        .copied()
                        .chain(file.lines().skip(self.line + 1)),
                    start + 2,
                    Some(Highlight {
                        range: start + 4..=start + 4,
                        color: Color::Green,
                    }),
                )
            }
            &Kind::WrongHeadingLevel {
                current_level,
                expected_level,
            } => {
                print_error(stderr, "Wrong heading level");
                let start = self.line.saturating_sub(2);
                self.print_path(stderr);
                display_original(stderr);

                let help = format!(
                    "Change {} to {}:",
                    "#".repeat(current_level),
                    "#".repeat(expected_level)
                );
                print_help(stderr, &help);
                let heading =
                    &file.lines().nth(self.line).unwrap()[current_level..];
                let correct_line =
                    format!("{}{}", "#".repeat(expected_level), heading);

                print_file(
                    stderr,
                    file.lines()
                        .skip(start)
                        .take(2)
                        .chain(once(correct_line.as_str()))
                        .chain(file.lines().skip(self.line + 1)),
                    start,
                    Some(Highlight {
                        range: self.line..=self.line,
                        color: Color::Green,
                    }),
                )
            }
            Kind::NoFigureImage => {
                print_error(stderr, "Expected an image in <figure>");
                let start = self.line.saturating_sub(2);
                self.print_path(stderr);
                display_original(stderr);

                print_help(stderr, "Add an image:");
                let tag_line = file.lines().nth(self.line).unwrap();
                print_file(
                    stderr,
                    [tag_line, "", "![](./images/{image_name}.png)"]
                        .iter()
                        .copied()
                        .chain(file.lines().skip(self.line + 1)),
                    start + 2,
                    Some(Highlight {
                        range: start + 4..=start + 4,
                        color: Color::Green,
                    }),
                )
            }
            Kind::NoFigcaption => {
                print_error(stderr, "Expected a <figcaption> in <figure>");
                self.print_path(stderr);
                display_original(stderr);

                print_help(stderr, "Add a caption:");
                print_file(
                    stderr,
                    "<figcaption>\n\n\
                     {caption}\n\n\
                     </figcaption>\n\n"
                        .lines(),
                    self.line,
                    Some(Highlight {
                        range: self.line..=self.line + 5,
                        color: Color::Green,
                    }),
                )
            }
            Kind::NoCaption => {
                print_error(stderr, "Expected a caption in <figcaption>");
                self.print_path(stderr);
                display_original(stderr);

                print_help(stderr, "Add a caption:");
                print_file(
                    stderr,
                    "<figcaption>\n\n\
                     {caption}\n\n\
                     </figcaption>\n\n"
                        .lines(),
                    self.line,
                    Some(Highlight {
                        range: self.line + 2..=self.line + 2,
                        color: Color::Green,
                    }),
                )
            }
            Kind::InvalidCaption => {
                print_error(stderr, "Invalid caption in <figcaption>");
                self.print_path(stderr);
                display_original(stderr);
                print_help(stderr, "A caption can only be a paragraph");
            }
            Kind::InvalidFigureEnd => {
                print_error(stderr, "Invalid end of a figure");
                self.print_path(stderr);
                display_original(stderr);
                print_help(
                    stderr,
                    "A figure can only end with </figcaption>\\n</figure>",
                );
            }
            Kind::UnknownHtml => {
                print_error(stderr, "Unknown HTML");
                self.print_path(stderr);
                display_original(stderr);

                print_help(stderr, "Only these HTML markups are allowed:");
                ALLOWED_HTML_MARKUP.iter().for_each(|markup| {
                    writeln!(stderr, "       {}", markup).unwrap();
                });
            }
            _ => (),
        }
    }

    fn print_path(&self, stderr: &mut StandardStream) {
        let mut gray = ColorSpec::new();
        gray.set_fg(Some(Color::Black));
        gray.set_intense(true);

        stderr.set_color(&gray).unwrap();
        write!(stderr, "   -->").unwrap();

        stderr.reset().unwrap();
        writeln!(stderr, " {}", self.path.to_string_lossy()).unwrap();
    }
}

fn print_error(stderr: &mut StandardStream, error: &str) {
    print_badge(stderr, "error", Color::Red);
    writeln!(stderr, "{}", error).unwrap();
}

fn print_help(stderr: &mut StandardStream, help: &str) {
    print_badge(stderr, "help", Color::Green);
    writeln!(stderr, "{}", help).unwrap();
    stderr.reset().unwrap();
}

fn print_badge(stderr: &mut StandardStream, badge: &'static str, color: Color) {
    stderr
        .set_color(ColorSpec::new().set_fg(Some(color)).set_bold(true))
        .unwrap();
    write!(stderr, "{:>5}", badge).unwrap();

    stderr.set_color(ColorSpec::new().set_bold(true)).unwrap();
    write!(stderr, ": ").unwrap();
}

fn print_file<'a, I, R>(
    stderr: &mut StandardStream,
    mut file: I,
    first_line_index: usize,
    highlight: Option<Highlight<R>>,
) where
    I: Iterator<Item = &'a str>,
    R: RangeBounds<usize>,
{
    let choose_color = |stderr: &mut StandardStream, index| {
        let mut spec = ColorSpec::new();
        spec.set_bold(true);
        match &highlight {
            Some(Highlight { range, color }) if range.contains(&index) => {
                spec.set_fg(Some(*color));
            }
            _ => {
                spec.set_fg(Some(Color::Black));
                spec.set_intense(true);
            }
        }

        stderr.set_color(&spec).unwrap();
    };

    if first_line_index > 0 {
        choose_color(stderr, first_line_index - 1);
        writeln!(stderr, " ... │ ").unwrap();
    }

    for (offset, line) in (&mut file).enumerate().take(5) {
        let line_index = offset + first_line_index;

        choose_color(stderr, line_index);
        write!(stderr, "{:>4} │ ", line_index + 1).unwrap();

        stderr.reset().unwrap();
        writeln!(stderr, "{}", line).unwrap();
    }

    if file.next().is_some() {
        choose_color(stderr, first_line_index + 6);
        writeln!(stderr, " ... │ ").unwrap();
        stderr.reset().unwrap();
    }
}
