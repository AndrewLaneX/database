# Contributing to the glossary database

We appreciate your will to contribute to the glossary! This reposity consists
of the database itself and the reference parser of the database, and there are
different ways to contribute to different parts. If you haven't worked with
Git/GitLab before, check out [this page][git-intro].

[git-intro]: https://docs.gitlab.com/ee/topics/git/index.html

## The database

### Amending the database with merge requests

If you want to fix a typo, incorrect or outdated information, or describe new
variables, you can open a merge request with the changes you want to make.
Here are the steps:

1. Fork the repository;
2. Create a branch with a name like `{language}-{summary}`;
3. Make the changes you want;
4. [Open][new-mr] a merge request against the database:to.
   - Summarize your changes in the title;
   - If your change is more than fixing a typo, give a description of your
     changes.
5. The maintainers of the amended glossary will review your changes. If
   everything is good, your merge request will be approved and merged;
   otherwise, they will ask you to make some changes before accepting your
   changes;
6. 🎉 Congratulations! Your contribution will be online in a few minutes after
   merging.

For every merge request, a _pipeline_ is started. The pipeline checks if your
changes are well-formatted and if they're syntactically valid. If the pipeline
fails, you need to fix the cause. To see the error, click the pipeline, and
then click on the failed job. If there's a syntax error, you'll also see what
is exactly wrong and how you can fix it. If your changes are formatted badly,
you'll see the exact files you need to format by running `npm run format`.

[new-mr]: https://gitlab.com/attheme-glossary/database/merge_requests/new

### Discussing problems in issues

If you want to discuss terminology, punctuation, some process involved
in creating the glossary or something else, [open an issue][new-issue]
in the repository:

- Give a summary in the title;
- Elaborate on the problem in the description.

If the problem is only related to a local glossary, you can discuss
the problem in the language of the glossary. People can freely participate
in the discussion you started. When there's a consensus, it may be worth
stating in the glossary's `Readme` file.

[new-issue]: https://gitlab.com/attheme-glossary/database/issues/new

## The parser

If you have a problem with the parser or found a bug, please
[file an issue][new-issue]. If you want to propose changes to it,
[open a merge request][new-mr]. Most of the previous section applies
here too.
