# Элементы управления

## Текстовые поля

Можно найти в Настройках → Никнейм.

- Цвет вводимого текста задается `windowBackgroundWhiteBlackText`.

<glossary-variable color="red">

### windowBackgroundWhiteHintText

Задает цвет вводимого заполнителя — это текст, который Вы видете перед тем, как
что-либо ввели.

</glossary-variable>

<glossary-variable color="indigo">

### windowBackgroundWhiteInputField

Задает цвет нижней границы, когда ввод неактивен.

</glossary-variable>

<glossary-variable color="green">

### windowBackgroundWhiteInputFieldActivated

Задает цвет нижней границы, когда ввод активен.

</glossary-variable>

<figure>

![](./images/controls.input.png)

<figcaption>

Красное — `windowBackgroundWhiteHintText`,
синее — `windowBackgroundWhiteInputField`,
зеленое — `windowBackgroundWhiteInputFieldActivated`.

</figcaption>
</figure>

## Переключатель

Можно найти в Настройках → Уведомления и звук.

<glossary-variable color="indigo">

### switchTrack

Задает цвет фона переключателя, когда переключатель выключен.

</glossary-variable>

<glossary-variable color="red">

### switchThumb

Задает цвет кнопки переключателя — кружочка на конце переключателя — когда он выключен.

</glossary-variable>

<glossary-variable color="teal">

### switchTrackChecked

Задает цвет фона переключателя, когда он включен.

</glossary-variable>

<glossary-variable color="pink">

### switchThumbChecked

Задает цвет кнопки переключателя, когда он включен.

</glossary-variable>

<figure>

![](./images/controls.switch.png)

<figcaption>

Красное — `switchThumb`, синее — `switchTrack`, зеленое — `switchTrackChecked`,
розовое — `switchThumbChecked`.

</figcaption>
</figure>

## Чекбокс

Можно найти в Информации чата → Уведомления → Настроить.

<glossary-variable color="red">

### checkboxSquareUnchecked

Задает обводку чекбокса, когда он без галки.

</glossary-variable>

<glossary-variable color="orange">

### checkboxSquareDisabled

Задает фон чекбокса, когда он отключен. Можно найти в настройках группы,
которая не переделана в супергруппу.

</glossary-variable>

<glossary-variable color="indigo">

### checkboxSquareBackground

Задает фон чекбокса с галкой.

</glossary-variable>

<glossary-variable color="green">

### checkboxSquareCheck

Задает цвет галки в чекбоксе.

</glossary-variable>

<figure>

![](./images/controls.checkbox.png)

<figcaption>

Красное — `checkboxSquareUnchecked`, синее — `checkboxSquareBackground`,
зеленое — `checkboxSquareCheck`, оранжевое — `checkboxSquareDisabled`.

</figcaption>
</figure>

## Выбор элемента из списка

Можно найти в Настройках → Телефон → Изменить номер.

<glossary-variable color="red">

### windowBackgroundGrayLine

Задает цвет обводки этого элемента.

</glossary-variable>

<figure>

![](./images/controls.selection.png)

<figcaption>

Красное — `windowBackgroundGrayLine`.

</figcaption>
</figure>

## Индикаторы прогресса

<glossary-variable color="red">

### contextProgressInner1

Задает цвет фона индикатора прогресса, когда измененное сообщение сохраняется.

</glossary-variable>

<glossary-variable color="red">

### contextProgressOuter1

Задает цвет заливки индикатора прогресса, когда измененное сообщение сохраняется.

</glossary-variable>

<glossary-variable color="green">

### contextProgressInner2

Задает цвет фона индикатора прогресса на панели действий при создании группы
или загрузке игры.

</glossary-variable>

<glossary-variable color="green">

### contextProgressOuter2

Задает цвет заливки индикатора прогресса на панели действий при создании группы
или загрузке игры.

</glossary-variable>

<glossary-variable color="indigo">

### contextProgressInner3

Задает цвет фона индикатора прогресса на верхней панели при открытии ссылки
с помощью Instant View. Эта переменная может быть изменена только с помощью
[`.attheme editor`].

</glossary-variable>

<glossary-variable color="indigo">

### contextProgressOuter3

Задает цвет заливки индикатора прогресса на верхней панели при открытии ссылки
с помощью Instant View. Эта переменная может быть изменена только с помощью
[`.attheme editor`].

</glossary-variable>

<glossary-variable color="amber">

### login_progressInner

Задает цвет фона индикатора прогресса времени таймера, когда Вы входите и ждете
смс от Telegram (обычно Telegram присылает смс, когда Вы вошли на другом
устройстве, но не имеете к нему доступ в данный момент).

</glossary-variable>

<glossary-variable color="amber">

### login_progressOuter

Задает цвет заливки индикатора прогресса времени таймера, когда Вы входите и
ждете смс от Telegram.

</glossary-variable>

<figure>

![](./images/controls.progress.0.png)

<figcaption>

Красное — `contextProgressInner1` и `contextProgressOuter1`,
зеленое — `contextProgressInner2` и `contextProgressOuter2`,
фиолетовое — `contextProgressInner3` и `contextProgressOuter3`,
оранжевое — `login_progressInner` и `login_progressOuter`.

</figcaption>
</figure>

[`.attheme editor`]: https://attheme-editor.snejugal.ru

<glossary-variable color="red">

### progressCircle

Задает цвет индикатора загрузки под списками.

</glossary-variable>

<figure>

![](./images/controls.progress.1.png)

<figcaption>

Красное — `progressCircle`.

</figcaption>
</figure>

## Кнопки действия

### Список чатов

<glossary-variable color="gray">

#### chats_actionBackground

Задает фон кнопки «Новое сообщение».

</glossary-variable>

<glossary-variable color="gray">

#### chats_actionPressedBackground

Задает цвет перекрытия кнопки «Новое сообщение», когда она нажата. Перекрытие
означает, что если вы поставите значение альфа канала меньше 255, то кнопка не
станет прозрачной — она будет сочетать два цвета. Это перекрывает только фон,
иконка остается.

</glossary-variable>

<glossary-variable color="gray">

#### chats_actionIcon

Задаёт цвет иконки на кнопке «Новое сообщение».

</glossary-variable>

### Экран профиля

<glossary-variable color="red">

#### profile_actionBackground

Задает фон кнопки.

</glossary-variable>

<glossary-variable color="amber">

#### profile_actionPressedBackground

Задает цвет перекрытия кнопки, когда она нажата.

</glossary-variable>

<glossary-variable color="green">

#### profile_actionIcon

Задаёт цвет иконки на кнопке панели действий. В группах, где вы являетесь
администратором, кнопка означает «Поменять аватарку группы», тогда как при
просмотре информации человека кнопка означает «Написать человеку», или
«Переместиться на своё местоположение» на экране прикрепления локации.

</glossary-variable>

<figure>

![](./images/controls.actionButton.png)

<figcaption>

Красное — `profile_actionBackground`,
оранжевое — `profile_actionPressedBackground`, зеленое — `profile_actionIcon`.

</figcaption>
</figure>
