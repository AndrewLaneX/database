# Фоны

<glossary-variable color="red">

## windowBackgroundWhite

Задаёт фон практически везде: например, фон списка чатов или настроек.

</glossary-variable>

<glossary-variable color="green">

## windowBackgroundGray

Задаёт второстепенный фон: например, фон в настройках между разделами,
или фон после истории звонков, если он не занимает весь экран.

</glossary-variable>

<figure>

![](./images/backgrounds.0.png)

<figcaption>

Красное ­— `windowBackgroundWhite`, зелёное — `windowBackgroundGray`.

</figcaption>
</figure>
