# Obsolete variables

These variable once were used, but now they've been removed from Telegram
because their elements were removed or replaced with other variables.

- `listSelector` — it was merged with `listSelectorSDK21`;
- `player_seekBarBackground` — the element was removed;
- `player_duration` — the element was merged with `player_time`.
