# The .attheme glossary database

The core of .attheme glossary.

## Layout

This directory contains subdirectories for every language, e.g. `en` and `ru`.
Every subdirectory has a file named `info.toml` which contains the very basic
strings used when rendering the website. It has these properties:

- `language_name`: how the language calls itself, e.g. for English it is
  _English_, for Russian it is _Русский_ and for Swedish it would be _Svenska_;
- `title`: the name of the glossary shown on the tab and in the header.
  For English, it is _.attheme glossary_;
- `search`: the placeholder shown in Search. For English, it is `Search…`.

Every top-level section of the glossary has a dedicated file named
`{n}.{name}.md`, where `n` is an ordinal number of the section, and `name`
is a snake_case name that describes the contents of the section. For example:

- `en`
  - `info.toml`
  - `0.prelude.md`
  - `1.backgrounds.md`

Every Markdown file looks like this:

```md
# Heading

A paragraph with **bold**, _italic_, `monospace` or ~~strikethrough~~ text.

## A subsection

1. An ordered
2. list with
3. several items

- A bullet
- list with
- several items

4. An ordered list can continue a previous one

<glossary-variable color="anycolor">

### variable

The description of the variable

</glossary-variable>

<figure>

![](./images/image_name.png)

<figcaption>

A description of the image, maybe pointing to other `variable`s.

</figcaption>
</figure>
```

Please note that the syntax for variables and figures must be exactly as
in the example above: at least one blank line between HTML tags and Markdown
markup, no unnecessary whitespace in HTML. The color of variables must be
one of these:

- `red`
- `pink`
- `purple`
- `deep-purple`
- `indigo`
- `blue`
- `light-blue`
- `cyan`
- `teal`
- `green`
- `light-green`
- `lime`
- `yellow`
- `amber`
- `orange`
- `deep-orange`
- `brown`
- `gray`
- `blue-gray`

You can see the colors and their HEX values in
[Material Design guidelines][colors]. Please use the 500 shade of the colors
when highlighting areas on screenshots.

Also, do not use more that five heading levels. While the files use
the headings `h1`..=`h5`, they're going to be rendered as `h2`..=`h6`.

[colors]: https://material.io/design/color/#tools-for-picking-colors
