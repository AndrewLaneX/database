use attheme_glossary_parser::parse;
use std::io::Write;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

fn main() {
    let database = std::fs::read_dir("../database").unwrap();
    let glossary_dirs = database.filter_map(|entry| {
        let entry = entry.ok()?;
        let file_type = entry.file_type().ok()?;

        if file_type.is_dir() {
            Some(entry)
        } else {
            None
        }
    });

    glossary_dirs.for_each(|glossary_dir| {
        let stderr = &mut StandardStream::stderr(ColorChoice::Always);

        let path = glossary_dir.path();
        write!(stderr, "Checking `{}`... ", path.to_string_lossy()).unwrap();
        stderr.flush().unwrap();

        let mut color = ColorSpec::new();
        color.set_bold(true);

        if let Err(errors) = parse(path) {
            color.set_fg(Some(Color::Red));
            stderr.set_color(&color).unwrap();

            writeln!(stderr, "Failed:").unwrap();
            stderr.set_color(&ColorSpec::new()).unwrap();

            errors.iter().for_each(|error| error.display());
        } else {
            color.set_fg(Some(Color::Green));
            stderr.set_color(&color).unwrap();

            writeln!(stderr, "Done").unwrap();
            stderr.set_color(&ColorSpec::new()).unwrap();
        }
    });
}
